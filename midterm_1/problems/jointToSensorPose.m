function [ T_IS ] = jointToSensorPose( q, params )
  % q: a 3x1 vector of generalized coordinates
  % params: a struct of parameters
  l0 = params.l0;
  l1 = params.l1;
  l2 = params.l2;
  l3 = params.l3;
  l41 = params.l41;
  l42 = params.l42;
  l5 = params.l5;
  l6 = params.l6;
  alpha = params.alpha;
  % rotating from I to 0 requires a rotation around x_I by pi, then a
  % rotation around the new z_I by -pi/2
  % we can treat it as the transformation from I to I', and I' to 0, where
  % the origin of I' is I, but rotated by 180 degrees
    T_I0 = [1 0 0 0;
            cos(pi) 0 -sin(pi) 0;
            sin(pi) 0 cos(pi) 0;
            0 0 0 1] * ...
            [cos(-pi/2) 0 -sin(-pi/2) 0;
            sin(-pi/2) 0 cos(-pi/2) 0;
            0 0 1 -l0;
            0 0 0 1];
    % rotation from 0 to 1 is around y
    T_01 = [cos(q(1)) 0 sin(q(1)) l1;
            0 1 0 0;
            -sin(q(1)) 0 cos(q(1)) 0;
            0 0 0 1];
    % rotation from 1 to 2 is around y
    T_12 = [cos(q(2)) 0 sin(q(2)) 0;
            0 1 0 0;
            -sin(q(2)) 0 cos(q(2)) l2;
            0 0 0 1];
    % rotation from 2 to 3 is around y
    T_23 = [cos(q(3)) 0 sin(q(3)) 0;
            0 1 0 0;
            -sin(q(3)) 0 cos(q(3)) l3;
            0 0 0 1];
    % rotation from 3 to the sensor has constant rotation and translation
    T_3S = [cos(pi-alpha) 0 sin(pi-alpha) l5 - l6*sin(alpha);
            0 1 0 0;
            -sin(pi-alpha) 0 cos(pi-alpha) l41 + l6*sin(alpha);
            0 0 0 1];
    % transformation from inertial frame to sensor frame
    T_IS = T_I0 * T_01 * T_12 * T_23 * T_3S;
            
end