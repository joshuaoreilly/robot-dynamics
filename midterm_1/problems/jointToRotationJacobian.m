function [ J_I2_r ] = jointToRotationJacobian(q, params)
  % q: a 3x1 vector of generalized coordinates
  % params: a struct of parameters
  l0 = params.l0;
  l1 = params.l1;
  l2 = params.l2;
  % reuse code from jointToPositionJacobian:
  % rotating from I to 0 requires a rotation around x_I by pi, then a
  % rotation around the new z_I (in our temporary frame a) by -pi/2
  % we can treat it as the transformation from I to a, and a to 0, where
  % the origin of a is I, but rotated by 180 degrees
    T_Ia = [1 0 0 0;
            cos(pi) 0 -sin(pi) 0;
            sin(pi) 0 cos(pi) 0;
            0 0 0 1];
    T_a0 = [cos(-pi/2) 0 -sin(-pi/2) 0;
            sin(-pi/2) 0 cos(-pi/2) 0;
            0 0 1 -l0;
            0 0 0 1];
    % rotation from 0 to 1 is around y
    T_01 = [cos(q(1)) 0 sin(q(1)) l1;
            0 1 0 0;
            -sin(q(1)) 0 cos(q(1)) 0;
            0 0 0 1];
    % rotation from 1 to 2 is around y
    T_12 = [cos(q(2)) 0 sin(q(2)) 0;
            0 1 0 0;
            -sin(q(2)) 0 cos(q(2)) l2;
            0 0 0 1];
    
    % Get transformation matrices with respect to intertial frame
    T_I0 = T_Ia * T_a0;
    T_I1 = T_I0 * T_01;
    T_I2 = T_I1 * T_12;
    
    % Get rotation matrices and translation vectors
    %C_Ia = T_Ia(1:3,1:3);
    C_I0 = T_I0(1:3,1:3);
    C_I1 = T_I1(1:3,1:3);
    C_I2 = T_I2(1:3,1:3);
    
    % Get rotation vectors (axis of rotation for each n) in inertial frame
    %n_Ia = C_Ia * [1 0 0]';
    n_I0 = C_I0 * [0 1 0]';
    n_I1 = C_I1 * [0 1 0]';
    n_I2 = C_I2 * [0 1 0]';
  
  % Implement your solution here...
  J_I2_r = [n_I0;
            n_I1;
            n_I2];
  
end