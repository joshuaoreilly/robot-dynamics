function [ Dq ] = kinematicTrajectoryControl( q, p_des, w_des, params )  
    % Inputs:
    %  q             : current joint angles (3x1)
    %  p_des         : desired gripper pose (3x1)
    %  w_des         : desired gripper twist (3x1)
    %  params        : a struct of parameters

    % Output:
    %  Dq            : joint velocity command (3x1)
    
    % Choose a proportional controller gain
    K_p = 2;
    
    % Choose a pseudo_inverse damping coefficient
    lambda = 2;
    
    % Implement your solution here...
    % couldn't find a set of gain and lambda that worked, but my solution
    % is probably just wrong overall
    
    p_current = jointTo2DGripperPosition_solution(q, params);
    % jointTo2DGripperPosition_solution returns a 2x1 vector instead of
    % 3x1, since it negects the y component. However, p_des and w_des are
    % both 3x1 matrices, so we need to reinsert the y component into
    % p_current
    p_current = [p_current(1) p_current(2) 0];
    J_analytic = jointToGripperAnalyticalJacobian_solution(q, params);
    v_command = w_des + K_p * (p_des - p_current);
    
    Dq = pseudoInverseMat_solution(J_analytic, lambda) * v_command;  
    
end