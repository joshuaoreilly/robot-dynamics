function [ T_CG ] = gripperToCameraPose(thetaZ, thetaY, thetaX, I_p_IC, T_IG)
  % thetaZ, thetaY, thetaX: ZYX Euler angles
  % I_p_IC : 3 x 1 camera position vector, expressed in the inertial frame
  % T_IG: 4 x 4 homogeneous transformation matrix of G with respect to I
  
  % Euler angle rotations are expressed as consecutive elementary rotations
  % around the z, y and x axis respectively.
  % we can produce the full rotation as:
  C_Z = [cos(thetaZ) 0 -sin(thetaZ);
        sin(thetaZ) 0 cos(thetaZ);
        0 0 1];
  C_Y = [cos(thetaY) 0 sin(thetaY);
        0 1 0 ;
        -sin(thetaY) 0 cos(thetaY)];
  C_X = [1 0 0;
        cos(thetaX) 0 -sin(thetaX);
        sin(thetaX) 0 cos(thetaX)];
  C_euler = C_Z * C_Y * C_X;
  
  % Then the transformation matrix from I to C
  T_IC = [C_euler, I_p_IC;
            0, 0, 0, 1];

  % Implement your solution here ...
  T_CG = T_IC' * T_IG;
end