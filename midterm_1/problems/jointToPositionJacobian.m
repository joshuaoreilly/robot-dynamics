function [ J_03_p ] = jointToPositionJacobian(q, params)
  % q: a 3x1 vector of generalized coordinates
  % params: a struct of parameters
  l1 = params.l1;
  l2 = params.l2;
  l3 = params.l3;
  % reuse code from jointToSensorPose:
    % rotation from 0 to 1 is around y
    T_01 = [cos(q(1)) 0 sin(q(1)) l1;
            0 1 0 0;
            -sin(q(1)) 0 cos(q(1)) 0;
            0 0 0 1];
    % rotation from 1 to 2 is around y
    T_12 = [cos(q(2)) 0 sin(q(2)) 0;
            0 1 0 0;
            -sin(q(2)) 0 cos(q(2)) l2;
            0 0 0 1];
    % rotation from 2 to 3 is around y
    T_23 = [cos(q(3)) 0 sin(q(3)) 0;
            0 1 0 0;
            -sin(q(3)) 0 cos(q(3)) l3;
            0 0 0 1];
    
    % Get transformation matrices with respect to 0 frame
    T_02 = T_01 * T_12;
    T_03 = T_02 * T_23;
    
    % Get rotation matrices and translation vectors
    C_01 = T_01(1:3,1:3);
    C_02 = T_02(1:3,1:3);
    C_03 = T_03(1:3,1:3);
    r_01 = T_01(1:3,4);
    r_02 = T_02(1:3,4);
    r_03 = T_03(1:3,4);
    
    % Get rotation vectors (axis of rotation for each n) in inertial frame
    n_01 = C_01 * [0 1 0]';
    n_02 = C_02 * [0 1 0]';
    n_03 = C_03 * [0 1 0]';
    
  % Implement your solution here...
  J_03_p = [cross(n_01, r_03 - r_01);
            cross(n_02, r_03 - r_02);
            cross(n_03, r_03 - r_03)];
  
end

