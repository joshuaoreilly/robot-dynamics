function [ pinvA ] = pseudoInverseMat(A, lambda)
% Input: Any m-by-n matrix, and a damping factor.
% Output: An n-by-m pseudo-inverse of the input according to the Moore-Penrose formula

% Get the number of rows (m) and columns (n) of A
[m, n] = size(A);

if n > m
    % redundant (more joints than degrees in task space), infinite solutions
    % A / B is apparently faster than A * inv(B)
    % no need to check if n == m, since if that is the case, then the
    % pseudo-inverse is identical to the true inverse, which is equal to
    % the transpose of the original. In this case, we get the added benefit
    % of keeping our damping factor
    pinvA = A' / (A*A' + lambda^2 .* eye(m));
else
    % overdetermined (not enough joints to describe task space), no exact
    % solution
    % A \ B is apparently faster than inv(A) * B
    pinvA = (A' * A + lambda^2 .* eye(n)) \ A';
end

end
