function T23 = jointToTransform23(q)
  % Input: joint angles
  % Output: homogeneous transformation Matrix from frame 3 to frame 2. T_23
  q = q(3);
  % rotation about y
  T23 = [cos(q) 0 sin(q) 0;
        0 1 0 0;
        -sin(q) 0 cos(q) 0.27;
        0 0 0 1];
end
