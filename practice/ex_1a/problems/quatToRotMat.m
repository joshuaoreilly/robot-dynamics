function R = quatToRotMat(q)
  % Input: quaternion [w x y z]
  % Output: corresponding rotation matrix
  
  q_0 = q(1);
  q_i = q(2:4);
  
  
  
  % PLACEHOLDER FOR OUTPUT -> REPLACE WITH SOLUTION
  %R = eye(3,3) + 2 .* q_0 * skew(q_i)^2;
  R = (2 .* q_0^2 - 1) .* eye(3) + 2 .* q_0 * skew(q_i) + 2 .* q_i * q_i';
end

function w_s = skew(w)
    w_s = [0 -w(3) w(2);
            w(3) 0 -w(1);
            -w(2) w(1) 0];
end