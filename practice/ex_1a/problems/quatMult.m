function q_AC = quatMult(q_AB,q_BC)
  % Input: two quaternions to be multiplied
  % Output: output of the multiplication
  
  q = q_AB;
  p = q_BC;
  
  q_w = q(1);
  q_n = q(2:4);
  p_w = p(1);
  p_n = p(2:4);
  
  %q_AC = [q_w, -(q_n');
  %      q_n, (q_w .* eye(3) - skew(q_n))] * p;
  q_AC = [q_w*p_w - q_n'*p_n;
             q_w*p_n + p_w*q_n + skew(q_n)*p_n];
end

function w_s = skew(w)
    w_s = [0 -w(3) w(2);
            w(3) 0 -w(1);
            -w(2) w(1) 0];
end