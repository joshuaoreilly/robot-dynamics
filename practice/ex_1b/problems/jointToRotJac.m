function J_R = jointToRotJac(q)
  % Input: vector of generalized coordinates (joint angles)
  % Output: Jacobian of the end-effector orientation which maps joint
  % velocities to end-effector angular velocities in I frame.

  % Step 1: transformation matrix from any joint frame to the inertial one
  T_I0 = getTransformI0();
  T_I1 = T_I0 * jointToTransform01(q);
  T_I2 = T_I1 * jointToTransform12(q);
  T_I3 = T_I2 * jointToTransform23(q);
  T_I4 = T_I3 * jointToTransform34(q);
  T_I5 = T_I4 * jointToTransform45(q);
  T_I6 = T_I5 * jointToTransform56(q);
  
  % Step 2: get rotation matrices and translation vectors
  % All are implicitely in inertial frame (no prefix I needed)
  %R_I0 = T_I0(1:3,1:3);
  R_I1 = T_I1(1:3,1:3);
  R_I2 = T_I2(1:3,1:3);
  R_I3 = T_I3(1:3,1:3);
  R_I4 = T_I4(1:3,1:3);
  R_I5 = T_I5(1:3,1:3);
  R_I6 = T_I6(1:3,1:3);
  
  r_I0 = T_I0(1:3,4);
  r_I1 = T_I1(1:3,4);
  r_I2 = T_I2(1:3,4);
  r_I3 = T_I3(1:3,4);
  r_I4 = T_I4(1:3,4);
  r_I5 = T_I5(1:3,4);
  r_I6 = T_I6(1:3,4);
    
  % Step 3: calculate the vectors from each joint to EE
  % since frame 6 = frame E, we can use r_I6
  r_1E = r_I6 - r_I1;
  r_2E = r_I6 - r_I2;
  r_3E = r_I6 - r_I3;
  r_4E = r_I6 - r_I4;
  r_5E = r_I6 - r_I5;
  r_6E = r_I6 - r_I6;
  
  % Step 4: compute the rotation vectors n_i in inertial frame
  % n_i are unit vectors representing axis of rotation at joint i
  n_1 = R_I1 * [0 0 1]';
  n_2 = R_I2 * [0 1 0]';
  n_3 = R_I3 * [0 1 0]';
  n_4 = R_I4 * [1 0 0]';
  n_5 = R_I5 * [0 1 0]';
  n_6 = R_I6 * [1 0 0]';
  
  % Compute the rotational jacobian.
  J_R = [n_1, n_2, n_3, n_4, n_5, n_6];

end
