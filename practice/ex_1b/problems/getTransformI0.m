function TI0 = getTransformI0()
  % Input: void
  % Output: homogeneous transformation Matrix from frame 0 to the inertial frame I. T_I0
  
  % Since I = 0, identity homoegenous matrix
  TI0 = [1 0 0 0;
         0 1 0 0;
         0 0 1 0;
         0 0 0 1];
end
