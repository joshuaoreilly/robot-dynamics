function T6E = getTransform6E()
  % Input: void
  % Output: homogeneous transformation Matrix from the end-effector frame E to frame 6. T_6E
  
  % since 6 = E, identity homogenous transformation matrix
  T6E = [1 0 0 0;
         0 1 0 0;
         0 0 1 0;
         0 0 0 1];
end

