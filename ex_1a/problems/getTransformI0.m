function TI0 = getTransformI0()
  % Input: void
  % Output: homogeneous transformation Matrix from frame 0 to the inertial frame I. T_I0
  
  % PLACEHOLDER FOR OUTPUT -> REPLACE WITH SOLUTION
  TI0 = [1 0 0 0;
        0 1 0 0;
        0 0 1 0;
        0 0 0 1];
  %TI0 = zeros(4);
end
