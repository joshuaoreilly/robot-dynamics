function T45 = jointToTransform45(q)
  % Input: joint angles
  % Output: homogeneous transformation Matrix from frame 5 to frame 4. T_45
  
  % PLACEHOLDER FOR OUTPUT -> REPLACE WITH SOLUTION
  T45 = [cos(q(5)) 0 sin(q(5)) 0.374-(0.134+0.072);
        0 1 0 0;
        -sin(q(5)) 0 cos(q(5)) 0;
        0 0 0 1];
  %T45 = zeros(4);
end

