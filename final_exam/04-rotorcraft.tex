\section{Rotorcraft}

Rotorcraft: aircraft with rotary wing turning in plane close to horizontal

Types: helicopter (main rotor active air), autogyro (passive main, active front, can't hover), gyrodyne (active top and rear gives extra propulsion), multirotor (high payload, inefficient), multicopter, omnidirectional multicopter, ducted fans (compact), coaxial (miniaturisation).

\subsection{Modelling Quadrotor}

Assumptions: CoF and body frame origin coincide, no surface interaction, structure rigid and symmetric, rigid propellers, no fuselage drag.

\includegraphics[width=\textwidth]{quadrotor.png}

Properties: arm length $l$, rotor height $j$, total lift mass $m$, inertia $I = \begin{bmatrix}I_{xx} & 0 & \\ 0 & I_{yy} & 0 \\ 0 & 0 & I_{zz}\end{bmatrix}$ with assumed symmetry in $xy$ and $yz$ plane.
Hub forces and rolling moments neglecting when hovering.

Rotation from body frame to earth frame (\textbf{left to right, left to right? Who knows}).

\[C_{EB} = C_{E1}(z,\psi)C_{12}(y,\theta)C_{2B}(x,\phi) = \begin{bmatrix}c\psi & -s\psi & 0 \\ s\psi & c\psi & 0 \\ 0 & 0 & 1\end{bmatrix}
											\begin{bmatrix}c\theta & 0 & s\theta \\ 0 & 1 & 0 \\ -s\theta & 0 & c\theta\end{bmatrix}
											\begin{bmatrix}1 & 0 & 0 \\ 0 & c\phi & -s\phi \\ 0 & s\phi & c\phi\end{bmatrix}\]

Attitude: represented with roll $-\pi < \phi < \pi$, pitch $-\pi/2 < \theta < \pi/2$ and yaw $-\pi < \psi < \pi$.
Angular velocity as fct. of roll-pitch-yaw derivatives w/ singularity at $\theta \pm 90^\circ$:

\[\theframe{B}\omega = \theframe{B}\begin{bmatrix}\omega_x \\ \omega_y \\ \omega_z\end{bmatrix} = \theframe{B}\begin{bmatrix}p \\ q \\ r \end{bmatrix}= E_r \dot{\chi}_r = \begin{bmatrix} 1 & 0 & -\sin\theta \\ 0 & \cos\psi & \sin\psi\cos\theta \\ 0 & -\sin\psi & \cos\psi\cos\theta \end{bmatrix} \begin{bmatrix}\dot{\phi} \\ \dot{\theta} \\ \dot{\psi}\end{bmatrix}\]

At hover, $E_r = E_{3\times3} = I_{3\times3}$, identity matrix as $\phi \approx \theta \approx 0$.

\subsubsection{Body Dynamics}

\[\begin{bmatrix} mE_{r} & 0 \\ 0 & I\end{bmatrix}
	\begin{bmatrix}\theframe{B}\dot{v} \\ \theframe{B}\dot{\omega}\end{bmatrix} + 
	\begin{bmatrix}\theframe{B}\omega \times m \theframe{B}v \\ \theframe{B}\omega\times I \theframe{B}\omega\end{bmatrix} = 
	\begin{bmatrix}\theframe{B}F \\ \theframe{B}M\end{bmatrix}\]

Forces and moments where $C_{EB}$ rotation matrix from body to earth fixed frame frame(\textbf{DOUBLE CHECK THIS, p.21}).

\[\theframe{B}F = \theframe{B}F_G + \theframe{B}F_{Aero} = C_{EB}^T \begin{bmatrix}0 \\ 0 \\ mg\end{bmatrix} + \sum_{i=1}^4 \begin{pmatrix}0 \\ 0 \\ -T_i\end{pmatrix}\]

\[\theframe{B}M_{Aero} = \theframe{B}M_T + \theframe{B}Q = \theframe{B}\begin{pmatrix} l(T_4 - T_2) \\ l(T_1-T_3) \\ 0 \end{pmatrix} +
					\theframe{B}\begin{pmatrix} 0 \\ 0 \\ \sum_{i=1}^4 Q_i(-1)^{(i-1)}\end{pmatrix}\]
					
where thrust forces $T_i$ are in shaft direction, $\theframe{B}M_T$ thrust-induced moments, $\theframe{B}Q$drag torques, thrust forces $T_i = b_i\omega_{p,i}^2$, drag forces $Q_i = d_i\omega_{p,i}^2$.

\subsubsection{Rotation Dynamics}

Top and bottom of dynamics decoupled; bottom row gives full control over all rotational speeds, isn't dependant on position states.

\begin{align*}
    I_{xx}\dot{\omega}_x&= \omega_y\cdot \omega_z (I_{yy}-I_{zz})+l\cdot b (\omega_{p,4}^2-\omega_{p,2}^2)\\
    I_{yy}\dot{\omega}_y&= \omega_z\cdot \omega_x (I_{zz}-I_{xx})+l\cdot b (\omega_{p,1}^2-\omega_{p,3}^2)\\
    I_{zz}\dot{\omega}_z&= d(\omega_{p,1}^2-\omega_{p,2}^2+\omega_{p,3}^2-\omega_{p,4}^2)\\
    &\text{with $\omega_{xyz}$ as entries of the body rotation ${}_\mathcal{B}\omega$}
\end{align*}

$(I_xx - I_yy) = 0$ (xy symmetry), dropped out of 3rd equation.

\subsubsection{Translation Dynamics}

Top row of dynamics only controls z-axis directly

\begin{align*}
    m\dot{v}_x =& m(\omega_z\cdot v_y - \omega_y\cdot v_z)\textcolor{blue}{-sin\theta mg}\\
    m\dot{v}_y =& m(\omega_x\cdot v_z - \omega_z\cdot v_x)\textcolor{blue}{+sin\phi cos\theta mg}\\
    m\dot{v}_z =& m(\omega_y\cdot v_x - \omega_x\cdot v_y) \textcolor{blue}{+cos\phi cos\theta mg }\\
    &- b(\omega_{p,1}^2+\omega_{p,2}^2+\omega_{p,3}^2+\omega_{p,4}^2)\\
    &\text{with gravitational Terms in \textcolor{blue}{blue}, } {}_\mathcal{B}F_G=C_{EB}^T\cdot {}_E\vec{n}_zmg 
\end{align*}

Note: To be consistent with the lecture notation: $(\omega_x, \omega_y, \omega_z) = (p,q,r) $ and $(v_x, v_y, v_z) = (u, v, w)$

\subsection{Control}

6-DoF system but 4 inputs (under-actuated); moving forward requires tipping around roll and pitch.

Assumptions: Neglect motor dynamics, low speeds \& linearise around small attitude angles (roll \& pitch).

\subsubsection{Control Overview}

Pair 1,3 and pair 2,4 rotate in opposite directions.
Vertical control (z-axis) by simultaneous rotor speed changes, direction control (around z) by imbalance between rotor pairs.
Longitudinal control (forward speed) by changing speeds of 1 and 3, lateral control (sideways speed) by changing speeds of 2 and 4.

\subsubsection{Virtual Control Input}

New set of inputs to decouple thrust ($U_1$) and moments ($U_2,U3,U4$, roll, pitch, yaw respectively)

\begin{align*}
	U_1 &= b(\omega_{p,1}^2 + \omega_{p,2}^2 + \omega_{p,3}^3 + \omega_{p,4}^2) \\
	U_2 &= l\cdot b(\omega_{p,4}^2 - \omega_{p,2}^2) \\
	U_3 &= l\cdot b(\omega_{p,1}^2 - \omega_{p,3}^2) \\
	U_4 &= d(-\omega_{p,1}^2 + \omega_{p,2}^2 - \omega_{p,3}^2 + \omega_{p,4}^2)
\end{align*}

\subsubsection{Linearization of Attitude Dynamics}

Linearizing about equilibrium point at hover ($\phi = \theta = \omega_{x,y,z}  = U_{2,3,4} = 0$, $U_1 = mg$, $E_r = I_{3\times3}$) gives three individual controllers:

\begin{align*}
	\dot{\omega}_x &= \ddot{\phi} = \frac{1}{I_{xx}}U_2 \\
	\dot{\omega}_y &= \ddot{\theta} = \frac{1}{I_{yy}}U_3 \\
	\dot{\omega}_z &= \ddot{\psi} = \frac{1}{I_{zz}}U_4
\end{align*}

\subsubsection{Attitude Control}

3 control loops (4th is altitude, see below)

\begin{align*}
	U_2 &= (\phi_{des} - \phi)k_{pRoll} - \dot{\phi}k_{dRoll} \\
	U_3 &= (\theta_{des} - \theta)k_{pPitch} - \dot{\theta}k_{dPitch} \\
	U_4 &= (\psi_{des} - \psi)k_{pYaw} - \dot{\psi}k_{dYaw}
\end{align*}

Relating virtual control inputs to actual motor control inputs:

\[\begin{bmatrix}U_1 \\ U_2 \\ U_3 \\ U_4 \end{bmatrix} = \begin{bmatrix} b & b& b & b \\ - & -lb & 0 & lb \\ lb & 0 & -lb & 0 \\ -d & d & -d & d\end{bmatrix}\begin{bmatrix}\omega_{p,1}^2 \\ \omega_{p,2}^2 \\ \omega_{p,3}^2 \\ \omega_{p,4}^2\end{bmatrix}\]

The matrix can be inverted to get the motor speeds from the virtual control inputs.

\subsubsection{Altitude Control}

Re-writing the above dynamics in the inertial frame:

\[\dot{v}_z = \ddot{z} = g - \cos\phi\cos\theta\frac{1}{b}U_1 = g - \frac{1}{m}T_z\]

where $T_z$ the thrust in inertial frame.
From this, we can get the input $U_1$ using a PD controller:

\[T_z = -k_p(z_{des} - z) + k_d\dot{z} - mg \rightarrow U_1 = \frac{T_z}{\cos\phi\cos\theta}\]

\subsubsection{Position Control}

Use 3 separate PD controllers to get thrust in x,y and z in inertial frame

\[\begin{bmatrix} \ddot{x} & \ddot{y} & \ddot{z}\end{bmatrix}^T = \frac{1}{m}\begin{bmatrix}T_x & T_y & T_z\end{bmatrix}^T + \begin{bmatrix}0 & 0 & g\end{bmatrix}^T\]

Transformed to get desired total thrust, and roll and pitch angles:

\[T = \sqrt(T_x^2 + T_y^2 + T_z^2) \quad \frac{1}{T}C_{E1}^T(z,\psi)\begin{bmatrix}T_x \\ T_y \\ T_z\end{bmatrix} = \begin{bmatrix}\sin\theta\cos\phi \\ -\sin\phi \\ \cos\theta\cos\phi\end{bmatrix}\]

\subsection{Propeller Aerodynamics}

Four main forces generated by rotor; thrust force (perpendicular to propeller place), drag torque (around rotor plane and opposite direction of rotor spinning), hub force (opposite to horizontal flight direction $V_H$, and rolling moment (around flight direction):

\begin{align*}
	|T| &= \frac{\rho}{2} A_P C_T(\omega_pR_P)^2\\
	|Q| &= \frac{\rho}{2} A_P C_Q(\omega_pR_P)^2\\
	|H| &= \frac{\rho}{2} A_P C_H (\omega_pR_P)^2\\
	|R| &= \frac{\rho}{2} A_P C_R (\omega_pR_P)^2R_P
\end{align*}

where $C_i$ force constant dependant on Blade pitch angle (geometry) and Reynolds number (propeller speed, velocity, rotational speed) for first two and advance ratio for the second two, $A_P$ rotor area, 

\subsubsection{Momentum Theory}

%%%%%%%%%%% GOT LAZY, COPIED SOMEONE ELSES SECTION %%%%%%%%%%%%%%%%%%%%%

Assumptions: infinitely thin rotor, thrust and velocity distribution uniform, quasi-static airflow, no viscous effects, incompressible air.
{\small
$\rho:\text{fluid density, }\vec{V}:\text{flow speed, } \vec{n}: \text{surface normal, }$ 

$dA: \text{surface Area patch, } p: \text{ surface pressure, } E: \text{ Energy, } P: \text{ Power}$
}

\smallskip
\begin{minipage}{0.6\linewidth}
    Conservation of fluid mass
    $$\displaystyle \iint \rho\vec{V}\cdot \vec{n}dA = 0 $$
    
    Conservation of fluid Momentum:
    $$\displaystyle \iint p\cdot \vec{n}dA+\iint (\rho\vec{V})\vec{V}\cdot \vec{n}dA = \vec{F} $$ 
    
    Conservation of energy:
    $$\displaystyle \iint\frac{1}{2}\rho V^2\vec{V}\cdot \vec{n}dA=\frac{dE}{dt}=P$$ 
    
\end{minipage}\hfill
\begin{minipage}{0.38\linewidth}
    Mass flow inside and outside (closed) control Volume must be equal
    
    \bigskip
    The net Force is the change of momentum of the fluid 
    
    \bigskip
    Work done on the fluid results in a gain of kinetic energy
\end{minipage}

\smallskip
\begin{minipage}{0.35\linewidth}
    \includegraphics[width=\linewidth]{rotoranalysis.png}
\end{minipage}\hfill
\begin{minipage}{0.6\linewidth}
    \textbf{1D Analysis:}
    
    \medskip
    The formulas lead to the following results:
    
    $\rho A_0 V = \rho A_R (V+u_1)$
    
    $\quad = \rho A_R (V+u_2)= \rho A_R (V+u_3)$
    
    $\Rightarrow u_1=u_2$
    
    \smallskip
    $F_{\textit{Thrust}} = \rho A_R(V+u_1)u_3$
    
    $P_{\textit{Thrust}} = F_{\textit{Thrust}}(V+u_1) $
    
    $\quad= \frac{1}{2}\rho A_R(V+u_1)(2V+u_3)u_3$
    
    $\Rightarrow = u_3 = 2u_1$
    
    \smallskip
    \textbf{In the Hover case ($V=0$):}
    
    Thrust Force: $F_{\textit{Thrust}} = 2\rho A_Ru_1^2$
    
    Slipstream Tube: $A_0 = \infty \quad A_3 = \frac{A_R}{2}$
\end{minipage}

Combining $P=F_{\textit{Thrust}}(\overbrace{V}^{=0}+u_1)$ with $F_{\textit{Thrust}}=2\rho A_Ru_1^2$ gives the ideal Power to Hover:
$$P=\frac{F_{\textit{Thrust}}^{3/2}}{\sqrt{2\rho A_R}} = \frac{(mg)^{3/2}}{\sqrt{2\rho A_R}} \text{ with } F_{\textit{Thrust}} = mg$$
The Power depends on the \textbf{Disc Loading} $F_{\textit{Thrust}}/A_R$

\smallskip
\textbf{Defining the rotor efficiency, Figure of Merit}, $FM$\\
$$FM = \frac{\textit{Ideal power to hover}}{\textit{Actual power to hover}}<1$$
$\rightarrow$ compare different propellers with the same disc loading.

\textbf{Add control diagram, showed up on exam}
\textbf{ADD CASE STUDY CONTENT IF YOU HAVE TIME?}
