function [ Dq ] = kinematicMotionControl(q, r_des, v_des)
% Inputs:
%  q          : current configuration of the robot
% r_des       : desired end effector position
% v_des       : desired end effector velocity
% Output: joint-space velocity command of the robot.

% Compute the updated joint velocities. This would be used for a velocity controllable robot

% linera position gain
K_p = 5;

% pseudo inverse damping coefficient
lambda = 0.1;

% get cartesian position of end effector
r_current = jointToPosition_solution(q);
% get jacobian of current configuration
J_current = jointToPosJac_solution(q);
% command velocity
v_command = v_des + K_p * (r_des - r_current);
Dq = pseudoInverseMat(J_current, lambda) * v_command;

end
