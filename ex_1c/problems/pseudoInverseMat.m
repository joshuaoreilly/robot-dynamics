function [ pinvA ] = pseudoInverseMat(A, lambda)
% Input: Any m-by-n matrix, and a damping factor.
% Output: An n-by-m pseudo-inverse of the input according to the Moore-Penrose formula

% Get the number of rows (m) and columns (n) of A
[m, n] = size(A);

% TODO: complete the computation of the pseudo-inverse.
% Hint: How should we account for both left and right pseudo-inverse forms?
if m > n
	% case of left pseudo inverse, used when problem is over-determined
	% (too many task variables, not enough joint variables)
	pinvA = inv(A.' * A + lambda^2 .* eye(n)) * A.';
else
	% case of right pseudo inverse, used when robot is redundant
	% (more joint space variables than task variables, infinite solutions)
	% if m == n, then right and left pseudo inverses are the same, so we just use this
	% not to mention just doing inv(A) might lead to singularity, so lambda solves that problem
	% thus making this solution "robust" against a rotation angle of zero (I think)
	pinvA = A.' * inv(A * A.' + lambda^2 .* eye(m));
end
